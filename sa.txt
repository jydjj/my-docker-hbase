
kafka:
image: wurstmeister/kafka
container_name: kafka
ports:
  - "9092:9092"
environment:
  - KAFKA_ZOOKEEPER_CONNECT=zookeeper:2181
  - KAFKA_ADVERTISED_LISTENERS=PLAINTEXT://服务器IP:9092
  - KAFKA_LISTENERS=PLAINTEXT://:9092
volumes:
  - /var/run/docker.sock:/var/run/docker.sock
restart: always