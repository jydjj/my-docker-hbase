[![Gitter chat](https://badges.gitter.im/gitterHQ/gitter.png)](https://gitter.im/big-data-europe/Lobby)

用的是大佬写好的docker-hive。
````
https://github.com/big-data-europe/docker-hive
````

# docker-hbase

# Standalone
To run standalone hbase:
```
docker-compose -f docker-compose-standalone.yml up -d
```
The deployment is the same as in [quickstart HBase documentation](https://hbase.apache.org/book.html#quickstart).
Can be used for testing/development, connected to Hadoop cluster.

# Local distributed
To run local distributed hbase:
```
docker-compose -f docker-compose-distributed-local.yml up -d
```

This deployment will start Zookeeper, HMaster and HRegionserver in separate containers.

# Distributed
To run distributed hbase on docker swarm see this [doc](./distributed/README.md):

一条命令实现停用并删除容器
```
docker stop $(docker ps -q) & docker rm $(docker ps -aq)
```
进入容器内部
````
docker exec -it hbase-master bash
````
启动对外访问
````
cd /bin  hbase-daemon.sh start thrift
````