
名词概念以及内存结构

<table>
    <tbody>
    <tr>
        <td rowspan="2">RowKey</td>
        <td colspan="2" >ColumnFamily : CF1</td>
        <td colspan="2" >ColumnFamily : CF2</td>
        <td rowspan="2" >TimeStamp</td>
    </tr>
    <tr>
        <td >Column: C11</td>
        <td >Column: C12</td>
        <td >Column: C21</td>
        <td >Column: C22</td>
    </tr>
    <tr>
        <td >“com.google”</td>
        <td >“C11 good”</td>
        <td >“C12 good”</td>
        <td >“C12 bad”</td>
        <td >“C12 bad”</td>
        <td >T1</td>
    </tr>
    </tbody>
</table>

<table>
    <tbody>
    <tr>
        <td rowspan="2">RowKey(行键，可理解成MySQL中的主键列)</td>
        <td colspan="3" >ColumnFamily(列族) : student</td>
        <td colspan="3" >ColumnFamily(列族) : teacher</td>
        <td rowspan="2" >TimeStamp(时间)</td>
    </tr>
    <tr>
        <td >Column(列): id</td>
        <td >Column(列): name</td>
        <td >Column(列): score</td>
        <td >Column(列): id</td>
        <td >Column(列): name</td>
        <td >Column(列): course</td>
    </tr>
    <tr>
        <td >“高三.1班”</td>
        <td >“id:1”</td>
        <td >“name:张三”</td>
        <td >“score:99”</td>
         <td >“id:15”</td>
        <td >“name:李老师”</td>
        <td >“course:语文”</td>
        <td >“2021年7月21日”</td>
    </tr>
    <tr>
        <td >“高三.1班”</td>
        <td >“id:1”</td>
        <td >“name:张三”</td>
        <td >“score:86”</td>
         <td >“id:15”</td>
        <td >“name:刘老师”</td>
        <td >“course:数学”</td>
        <td >“2021年7月21日”</td>
    </tr>
    </tbody>
</table>
