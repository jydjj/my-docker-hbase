#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import configparser
import json
import os

import happybase

# 单条数据插入HBase

cf = configparser.ConfigParser()
cf.read("config.ini")

class HBaseConnection:
    def __init__(self):
        # 初始化Thrift链接
        self.connection = happybase.Connection(host=cf.get("info", "thrift_host"),port= 9090)
        self.connection.open()
        # 打开表
        self.table = happybase.Table(cf.get("info", "table_name"), self.connection)

    def get_connection(self):
        return self.connection

    def get_table(self):
        return self.table

    def close(self):
        self.connection.close()


def get_json_tuple():
    dict_list = []
    fs = os.listdir(os.getcwd())
    for html_name in fs:
        if html_name.endswith(".json"):
            json_str = (open(html_name, encoding='UTF-8').read())
            dict_list.append(json.loads(json_str))
    return dict_list


def drop_table(name):
    hp = HBaseConnection()
    conn = hp.connection
    conn.disable_table(name)
    conn.delete_table(name)

if __name__ == '__main__':
    hp = HBaseConnection()
    conn = hp.connection
    # drop_table("表名")
    conn.create_table('表名', {"列族1": {},"列族2":{}})
    table = conn.table("表名")
    table.put("列", {"列族1:列1": "列族1列1值"})
    table.put("列", {"列族2:列1": "列族2列1值"})

    row = table.rows('列')
    print(row.values())


